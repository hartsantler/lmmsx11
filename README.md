# lmmsx11

lmms optimized for linux, X11, scripting, mplayer, and gamepad input.

# install ubuntu

sudo apt-get install libfftw3-dev libsamplerate-dev libsndfile-dev libfluidsynth-dev fluidsynth libfltk1.3-dev

# install fedora

sudo dnf install qt-devel libsndfile-devel libsamplerate-devel SDL2-devel
sudo dnf install http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install mplayer

# Scripting

Scripts can be executed from the command line using the --eval or --script arguments. Example: lmms --eval "print('hello world')"


# Standard Input Example

You can pipe commands into lmms from a parent process like this:

```javascript
var pattern = null;
var alpha = 'abcdefghijklmnopqrstuvwxyz';

if (! lmms.song.getInstrumentTrack(0).numOfTCOs()) {
	print('making new pattern...');
	lmms.song.getInstrumentTrack(0).newPattern();
}
pattern = lmms.song.getInstrumentTrack(0).getPattern(0);

lmms.onStdin.connect(
	function (key) {
		print('js:' + key);
		if (pattern) {
			var i = alpha.indexOf(key[1]) +64;
			if (key[0] == "+"){
				pattern.getInstrumentTrack().getPianoModel().handleKeyPress( i );
			} else {
				pattern.getInstrumentTrack().getPianoModel().handleKeyRelease( i );
			}
		} else {
			print('no pattern...')
		}
	}
);
```

# Lmms Scripting API

Below are all slots, signals, and properties exposed to the scripting system. It is almost exactly the same as the C++ API, except for a few cases like the Note class (which is not a subclass of QObject). The gamepad is also exposed to the scripting system, you can get the value of each analog axis using: lmms.gamepadAxis(n).
lmms

bbTrackContainer,deleteLater(),destroyed(),destroyed(QObject*),fxMixer,initProgress(QString),mixer,objectName,objectNameChanged(QString),song,gamepadAxis(int)
lmms.song

addBBTrack(),clearProject(),controllerAdded(Controller*),controllerRemoved(Controller*),dataChanged(),dataUnchanged(),deleteLater(),destroyed(),destroyed(QObject*),getBBTrack(int),getInstrumentTrack(int),getNumTracks(),lengthChanged(int),modified(),objectName,objectNameChanged(QString),playAndRecord(),playBB(),playPattern(const Pattern*),playPattern(const Pattern*,bool),playSong(),playbackPositionChanged(),playbackStateChanged(),projectFileNameChanged(),projectLoaded(),propertiesChanged(),record(),setModified(),startExport(),stop(),stopExport(),stopped(),tempoChanged(bpm_t),timeSignatureChanged(int,int),togglePause(),trackAdded(Track*),updateSampleTracks()
InstrumentTrack

dataChanged(),dataUnchanged(),deleteLater(),deleteTCOs(),destroyed(),destroyed(QObject*),destroyedTrack(),getPattern(int),instrumentChanged(),midiNoteOff(Note),midiNoteOn(Note),nameChanged(),newNote(),newPattern(),numOfTCOs(),objectName,objectNameChanged(QString),propertiesChanged(),setName(QString),toggleSolo(),trackContentObjectAdded(TrackContentObject*),updateBaseNote(),updateEffectChannel(),updatePitch(),updatePitchRange()
Pattern

addSteps(),changeTimeSignature(),clear(),cloneSteps(),copy(),dataChanged(),dataUnchanged(),deleteLater(),destroyed(),destroyed(QObject*),destroyedPattern(Pattern*),destroyedTCO(),getNote(int),getNoteAtStep(),getNoteAtStep(int),getNumNotes(),lengthChanged(),newNote(),newNote(int),newNote(int,int),newNote(int,int,int),objectName,objectNameChanged(QString),paste(),positionChanged(),propertiesChanged(),removeSteps(),setNote(NoteScriptWrapper*),toggleMute()
Note

deleteLater(),destroyed(),destroyed(QObject*),getKey(),objectName,objectNameChanged(QString),setKey(int)

Example script that will create a new pattern in the first default instrument track, then generate random notes, and continuously randomize the note values.

save below into myscript.js and run it with: lmms --script myscript.js

```javascript
setInterval( 
	function(){ 
		if (! lmms.song.getInstrumentTrack(0).numOfTCOs()) {
			lmms.song.getInstrumentTrack(0).newPattern();
		}
		var pat = lmms.song.getInstrumentTrack(0).getPattern(0);
		if (! pat.getNumNotes()) {
			for (var i=0; i<50; i++) {
				pat.newNote(i*10, 50*random() );
			}
		}
		for (var i=0; i<pat.getNumNotes(); i++) {
			var note = pat.getNote(i);
			if (random() > 0.5) {
				note.setKey( (8*random())+64 );
			}
		}
	},
	30
);
```

# lmms.newProcess

lmms.newProcess(exe, args, width, height) allows you to spawn a subprocess.

When running with X11 lmms.newProcess will also allow you to embed an external window (by XID) into the lmms workspace, the default width and height parameters are optional.
embed mplayer example

run this example using lmms from the command line with the --eval option.

```
lmms --eval "lmms.newProcess('mplayer', ['/path/to/myfile.mp4','-ao','sdl', '-vo', 'x11', '-nojoystick', '-noconsolecontrols', '-nomouseinput', '-wid'])"
```




